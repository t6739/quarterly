# Reports

a repository for different summary reports pertaining to Gimbal Labs endevours

including quarterly report - resource allocation disclosure


tokenomics - relates to crypto currency incentive design, technical implementation and other related aspects


outline funds allocation strategy dating back to fund 2 (use tables for the numbers)

create a discloure card for video content to give context - who's who, level of skin in the game, compensation, conflict of interests, how to contribute, how to meet monetary compensation contribution threshold… etc

this is a neat reference (https://www.apple.com/uk/privacy/labels/) "Transparency is the best policy."


stakepool costs and income

dandelion?

leadeship vs not leadership

protecting identity - ADA into ERGO mixer back into ADA







## Add your files

- [ ] [Create](https://gitlab.com/-/experiment/new_project_readme_content:dd467b9ab9f19bd66ec6c83023c0224c?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://gitlab.com/-/experiment/new_project_readme_content:dd467b9ab9f19bd66ec6c83023c0224c?https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://gitlab.com/-/experiment/new_project_readme_content:dd467b9ab9f19bd66ec6c83023c0224c?https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
code block
```



## Name

## Description

## Badges

consider inclusion of custom badges for broader community contribution recognition. could take form of a token and gltf. possibly aesthetically fashioned after these (https://i.pinimg.com/originals/85/2d/33/852d33a81ba89f2c93abce83e5cc9ffb.jpg)

## Visuals

## Usage

## Support

## Roadmap

## Contributing

## Authors and acknowledgment

## License

## Project status

Thank you to [makeareadme.com](https://www.makeareadme.com) for this template.
